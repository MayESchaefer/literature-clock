# Literature Clock
Clock using time quotes from the literature, based on work and idea by
        [Jaap Meijers](http://www.eerlijkemedia.nl/) ([E-reader clock](https://www.instructables.com/id/Literary-Clock-Made-From-E-reader/)).
        
The working site is in the `public/` folder, and can be visited at https://MayESchaefer.gitlab.io/literature-clock.
